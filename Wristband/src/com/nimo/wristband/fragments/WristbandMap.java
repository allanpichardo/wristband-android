package com.nimo.wristband.fragments;

import android.content.Intent;
import android.net.Uri;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nimo.wristband.net.Show;

public class WristbandMap extends MapFragment{

	private Show show;

	public void setShow(Show show){
		this.show = show;
		centerOnVenue();
	}

	private void centerOnVenue(){
		GoogleMap map = getMap();

		MarkerOptions marker = new MarkerOptions();
		marker.position(show.getLocation());
		marker.title(show.getVenueName());
		marker.draggable(false);

		map.addMarker(marker);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(show.getLocation(), 15));

	}
	
	public void setOnMarkerClickListener(OnMarkerClickListener listener){
		getMap().setOnMarkerClickListener(listener);
	}

}
