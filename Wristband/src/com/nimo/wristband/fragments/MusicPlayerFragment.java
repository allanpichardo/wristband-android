package com.nimo.wristband.fragments;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.nimo.wristband.R;
import com.nimo.wristband.net.BitmapLruCache;
import com.nimo.wristband.net.Show;
import com.nimo.wristband.service.WristbandPlayerService;
import com.nimo.wristband.service.WristbandPlayerService.PlaybackCallbacks;
import com.nimo.wristband.service.WristbandPlayerService.PlayerBinder;

public class MusicPlayerFragment extends Fragment implements ServiceConnection,
PlaybackCallbacks, OnClickListener,OnSeekBarChangeListener{

	private Context context;
	private WristbandPlayerService service;
	private Show show;
	private RequestQueue requestQueue;
	private ImageLoader imageLoader;
	private MusicPlayerCallbacks callback;

	private TextView bandText;
	private TextView albumText;
	private TextView titleText;
	private NetworkImageView smallCover;
	private ImageButton playButton;
	private ImageButton stopButton;
	private SeekBar seekBar;

	private boolean isBound = false;
	
	private int index = 0;
	
	public MusicPlayerFragment(){

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.media_player, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		context = getActivity();

		bandText = (TextView)getView().findViewById(R.id.bandText);
		albumText = (TextView)getView().findViewById(R.id.albumText);
		titleText = (TextView)getView().findViewById(R.id.titleText);
		smallCover = (NetworkImageView)getView().findViewById(R.id.smallCover);
		playButton = (ImageButton)getView().findViewById(R.id.playButton);
		stopButton = (ImageButton)getView().findViewById(R.id.stopButton);
		seekBar = (SeekBar)getView().findViewById(R.id.seekBar);

		requestQueue = Volley.newRequestQueue(context);
		imageLoader = new ImageLoader(requestQueue, new BitmapLruCache());

		playButton.setOnClickListener(this);
		stopButton.setOnClickListener(this);
		seekBar.setOnSeekBarChangeListener(this);

		seekBar.setMax(100);

		if(savedInstanceState != null){
			String s = savedInstanceState.getString(WristbandPlayerService.ARG_SHOW);
			JSONObject json = new JSONObject();
			try {
				json = new JSONObject(s);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			show = new Show(json);
			
		}
		
		smallCover.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(callback != null){
					callback.onIconClick();
				}
			}
		});
	}
	
	public void setMusicPlayerCallbacks(MusicPlayerCallbacks callback){
		this.callback = callback;
	}

	public void setShow(Show show){
		this.show = show;
		cue(true);
		if(callback != null){
			callback.onShowPlayer();
		}
	}
	
	public void setIndex(int index){
		this.index = index;
	}
	
	public int getIndex(){
		return this.index;
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		PlayerBinder binder = (PlayerBinder) service;
		this.service = binder.getService();
		isBound = true;

		this.service.setPlaybackCallbacks(this);

		if(!this.service.isPlaying()){
			cue(true);
		}else{
			backToFront();
		}
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		isBound = false;
	}

	@Override
	public void onPositionChange(int position, int duration) {
		float p = ((float)position/(float)duration);
		int percent = (int) (p * 100);
		seekBar.setProgress(percent);
	}

	@Override
	public void onPrepared() {
		service.play();
		//change play icon to pause icon
		playButton.setImageResource(android.R.drawable.ic_media_pause);
	}

	@Override
	public void onBufferingUpdate(int percent) {
		//seekBar.setMax(100);
		seekBar.setSecondaryProgress(percent);
	}

	@Override
	public void onCompleted() {
		stop();
	}

	private void stopService(){
		if (isBound) {
			getActivity().unbindService(this);
			isBound = false;
		}

		getActivity().stopService(new Intent(context,WristbandPlayerService.class));
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.playButton:
			//play
			cue(false);
			break;
		case R.id.stopButton:
			//stop
			stop();
			break;
		}
	}

	private void stop(){
		if(service != null){
			if(service.isInitialized()){
				if(service.isPlaying()){
					service.stop();
					playButton.setImageResource(android.R.drawable.ic_media_play);
				}
			}

			stopService();
			seekBar.setProgress(0);
			seekBar.setSecondaryProgress(0);
			if(callback != null){
				callback.onHidePlayer();
			}
		}
	}

	private void cue(boolean newShow){
		if(show != null){
			if(service != null && service.isInitialized() && service.isPlaying() && !newShow){
				//should pause
				service.pause();
				playButton.setImageResource(android.R.drawable.ic_media_play);
			}else if(service != null && service.isInitialized() && !service.isPlaying() && !newShow){
				service.play();
				playButton.setImageResource(android.R.drawable.ic_media_pause);
			}else{
				//should get ready to play
				if(!isBound){
					//need to bind first
					Intent intent = new Intent(context,WristbandPlayerService.class);
					getActivity().bindService(intent,this,Context.BIND_AUTO_CREATE);
				}else{
					//already bound, give the service the new show
					service.cue(show);
				}

				bandText.setText(show.getBandName());
				albumText.setText(show.getAlbumTitle());
				titleText.setText(show.getTrackTitle());
				smallCover.setImageUrl(show.getSmallArtUrl(),imageLoader);
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(show != null){
			outState.putString(WristbandPlayerService.ARG_SHOW, show.toString());
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		backToFront();
	}

	private void backToFront(){
		if(show != null){
			bandText.setText(show.getBandName());
			albumText.setText(show.getAlbumTitle());
			titleText.setText(show.getTrackTitle());
			smallCover.setImageUrl(show.getSmallArtUrl(),imageLoader);

			if(service.isPlaying()){
				if(!isBound){
					Intent intent = new Intent(context,WristbandPlayerService.class);
					getActivity().bindService(intent,this,Context.BIND_AUTO_CREATE);
				}
			}
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if(fromUser){
			if(service != null){
				int time = (int)(((float)progress / 100) * service.getDuration());
				service.seekTo(time);
			}else{
				seekBar.setProgress(0);
			}
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}
	
	public interface MusicPlayerCallbacks{
		public void onShowPlayer();
		public void onHidePlayer();
		public void onIconClick();
	}

}
