package com.nimo.wristband.adapters;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.nimo.wristband.R;
import com.nimo.wristband.db.ShowData;
import com.nimo.wristband.db.ShowData.OnShowsLoadedListener;
import com.nimo.wristband.net.BitmapLruCache;
import com.nimo.wristband.net.Show;
import com.nimo.wristband.net.WristbandResponse;

public class ShowListAdapter extends BaseAdapter implements OnShowsLoadedListener, OnScrollListener{

	private int currentVisibleItemIndex;
	private int currentVisibleItemCount;
	private int currentScrollState;
	private boolean isLoading = false;

	private Context context;
	private List<Show> shows;
	private double latitude;
	private double longitude;
	private ShowData showData;

	private ImageLoader imageLoader;
	private OnLoadingListener loadingListener;
	private ButtonCallbacks callback;

	public ShowListAdapter(Context context, double latitude, double longitude, OnLoadingListener listener){
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumIntegerDigits(2);

		Calendar calendar = Calendar.getInstance(Locale.US);
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		String month = nf.format(calendar.get(Calendar.MONTH)+1);
		String date = nf.format(calendar.get(Calendar.DATE));
		String pollDate = year+"-"+month+"-"+date;

		init(context,latitude,longitude,pollDate,listener);
	}

	public ShowListAdapter(Context context, double latitude, double longitude, String pollDate,OnLoadingListener listener){
		init(context,latitude,longitude,pollDate,listener);
	}

	private void init(Context context, double latitude, double longitude, String pollDate,OnLoadingListener listener){
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
		this.shows = new ArrayList<Show>();
		this.showData = new ShowData(context,pollDate);
		this.showData.setOnShowsLoadedListener(this);
		this.loadingListener = listener;

		RequestQueue requestQueue = Volley.newRequestQueue(context);
		imageLoader = new ImageLoader(requestQueue,new BitmapLruCache());
		//get what we have for now
		this.shows = showData.getShows();
		if(shows.size() == 0){
			loadMoreData();
		}
	}
	
	public void setButtonCallbacks(ButtonCallbacks callback){
		this.callback = callback;
	}
	
	public void setOnLoadingListener(OnLoadingListener listener){
		loadingListener = listener;
	}

	@Override
	public int getCount() {
		return shows.size();
	}

	@Override
	public Object getItem(int position) {
		return shows.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.show_list_item, null);
		}

		TextView bandNameText = (TextView)convertView.findViewById(R.id.bandNameText);
		TextView venueNameText = (TextView)convertView.findViewById(R.id.venueText);
		NetworkImageView coverImage = (NetworkImageView)convertView.findViewById(R.id.coverImage);
		ImageButton playButton = (ImageButton)convertView.findViewById(R.id.playButton);
		ImageButton shareButton = (ImageButton)convertView.findViewById(R.id.shareButton);
		ImageButton	infoButton = (ImageButton)convertView.findViewById(R.id.infoButton);

		final Show show = (Show)getItem(position);

		bandNameText.setText(show.getBandName());
		venueNameText.setText(show.getVenueName());
		if(show.getLargeArtUrl().length() > 0){
			coverImage.setImageUrl(show.getLargeArtUrl(), imageLoader);
		}
		
		playButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(callback != null){
					callback.onClickPlay(show);
				}
			}
		});
		shareButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(callback != null){
					callback.onClickShare(show);
				}
			}
		});
		infoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(callback != null){
					callback.onClickInfo(show);
				}
			}
		});

		return convertView;
	}

	@Override
	public void onShowsLoaded(WristbandResponse response) {
		if(response != null){
			if(response.isSuccess()){
				shows.addAll(response.getShows());
				notifyDataSetChanged();
			}
		}
		loadingListener.onLoadingComplete();
		isLoading = false;
	}


	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		this.currentVisibleItemCount = visibleItemCount;
		this.currentVisibleItemIndex = firstVisibleItem;
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		this.currentScrollState = scrollState;
		this.isScrollCompleted();
	}

	private void isScrollCompleted() {
		if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
			/*** In this way I detect if there's been a scroll which has completed ***/
			/*** do the work for load more date! ***/
			if(currentVisibleItemIndex == (getCount() - 2)){
				if(!isLoading){
					isLoading = true;
					loadMoreData();
				}
			}
		}
	}

	private void loadMoreData(){
		isLoading = true;
		loadingListener.onLoading();
		showData.fetch(latitude, longitude);
	}
	
	public interface OnLoadingListener{
		public void onLoading();
		public void onLoadingComplete();
	}
	
	public interface ButtonCallbacks{
		public void onClickPlay(Show show);
		public void onClickShare(Show show);
		public void onClickInfo(Show show);
	}

}
