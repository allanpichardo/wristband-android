package com.nimo.wristband.service;

import java.io.IOException;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.nimo.wristband.MainActivity;
import com.nimo.wristband.R;
import com.nimo.wristband.net.BitmapLruCache;
import com.nimo.wristband.net.Show;

public class WristbandPlayerService extends Service implements OnPreparedListener, 
OnErrorListener,OnAudioFocusChangeListener,OnBufferingUpdateListener,
OnInfoListener,OnCompletionListener,OnSeekCompleteListener{
	
	public static final String ARG_SHOW = "show";
	
	private final IBinder binder = new PlayerBinder();
	
	private MediaPlayer mediaPlayer = null;
	private PlaybackCallbacks listener;
	
	public static final long POLL_INTERVAL = 1000 * 1;
	private Handler positionHandler = new Handler();
	private Runnable positionRunnable = new Runnable() {
		
		@Override
		public void run() {
			if(mediaPlayer != null){
				try{
				if(mediaPlayer.isPlaying()){
					if(listener != null){
						listener.onPositionChange(mediaPlayer.getCurrentPosition(), mediaPlayer.getDuration());
					}
				}
				}catch(IllegalStateException e){
					
				}
			}
			positionHandler.postDelayed(this, POLL_INTERVAL);
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		init();
		return binder;
	}
	
	public class PlayerBinder extends Binder{
		public WristbandPlayerService getService(){
			return WristbandPlayerService.this;
		}
	}
	
	public void setPlaybackCallbacks(PlaybackCallbacks listener){
		this.listener = listener;
	}
	
	@Override
	public void onDestroy() {
		if(mediaPlayer != null){
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	private void init(){
		
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnInfoListener(this);
		mediaPlayer.setOnSeekCompleteListener(this);
	}
	
	private void showNotification(Show show){
		// assign the song name to songName
		
		Intent intent = new Intent(getApplicationContext(),MainActivity.class);
		intent.putExtra(ARG_SHOW, show.toString());
		
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
		                intent, PendingIntent.FLAG_UPDATE_CURRENT);
		//final Notification notification = new Notification();
		final Notification.Builder builder = new Notification.Builder(this);
		builder.setTicker(show.getBandName()+"-"+show.getTrackTitle());
		//notification.tickerText = show.getBandName()+"-"+show.getTrackTitle();
		builder.setOngoing(true);
		//notification.flags |= Notification.FLAG_ONGOING_EVENT;
		builder.setContentText("Playing: " + show.getTrackTitle());
		builder.setContentTitle(show.getBandName());
		//notification.setLatestEventInfo(getApplicationContext(), "Wristband",
		//               "Playing: " + show.getTrackTitle(), pi);
		builder.setSmallIcon(R.drawable.actionicon);
		builder.setContentIntent(pi);
		ImageLoader il = new ImageLoader(Volley.newRequestQueue(this),new BitmapLruCache());
		il.get(show.getSmallArtUrl(), new ImageListener() {
			
			@Override
			public void onErrorResponse(VolleyError error) {
				startForeground(111,builder.build());
			}
			
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				if(response.getBitmap() != null){
					builder.setLargeIcon(response.getBitmap());
					startForeground(111,builder.build());
				}
			}
		});
		
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		if(listener != null){
			listener.onPrepared();
		}
	}
	
	public boolean isPlaying(){
		try{
		return mediaPlayer.isPlaying();
		}catch(NullPointerException npe){
			stopSelf();
			return false;
		}
	}
	
	public boolean isInitialized(){
		return mediaPlayer != null;
	}
	
	public void seekTo(int position){
		mediaPlayer.seekTo(position);
	}
	
	public void cue(Show show){
		if(mediaPlayer.isPlaying()){
			mediaPlayer.stop();
		}
		mediaPlayer.reset();
		try {
			mediaPlayer.setDataSource(show.getTrackStreamingUrl());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mediaPlayer.prepareAsync();
		positionHandler.post(positionRunnable);
		showNotification(show);
	}
	
	public void stop(){
		if(mediaPlayer.isPlaying()){
			mediaPlayer.stop();
		}
		mediaPlayer.release();
		stopForeground(true);
	}
	
	public void play(){
		mediaPlayer.start();
	}
	
	public void pause(){
		mediaPlayer.pause();
	}
	
	public int getDuration(){
		return mediaPlayer.getDuration();
	}
	
	public interface PlaybackCallbacks{
		public void onPositionChange(int position, int duration);
		public void onPrepared();
		public void onBufferingUpdate(int percent);
		public void onCompleted();
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		if(listener != null){
			listener.onBufferingUpdate(percent);
		}
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		if(listener != null){
			listener.onCompleted();
		}
		positionHandler.removeCallbacks(positionRunnable);
	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onSeekComplete(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
	}

}
