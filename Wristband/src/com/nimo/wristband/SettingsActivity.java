package com.nimo.wristband;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.nimo.wristband.db.ShowData;

public class SettingsActivity extends Activity{
	
	private Button wipeButton;
	private Button feedbackButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		wipeButton = (Button)findViewById(R.id.wipeButton);
		feedbackButton = (Button)findViewById(R.id.feedbackButton);
		
		getActionBar().setTitle("Settings");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		wipeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				wipeData();
			}
		});
		feedbackButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sendFeedback();
			}
		});
		
	}
	
	private void sendFeedback(){
		startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:imaginarymercenary@gmail.com")));
	}
	
	private void wipeData(){
		ShowData showData = new ShowData(this);
		showData.wipeAll();
		Toast.makeText(this, "All data has been wiped.", Toast.LENGTH_SHORT).show();
	}

}
