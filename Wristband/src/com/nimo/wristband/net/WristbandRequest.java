package com.nimo.wristband.net;

import java.util.Map;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.nimo.wristband.constants.Constants;

public class WristbandRequest extends StringRequest{

	private Map<String,String> parameters;
	private RetryPolicy policy;

	public WristbandRequest(Map<String,String> parameters, Listener<String> listener,
			ErrorListener errorListener) {
		super(Method.POST, Constants.URL_WRISTBAND, listener, errorListener);
		this.parameters = parameters;
		policy = new DefaultRetryPolicy(1000 * 30, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
		
		printDebug();
	}
	
	private void printDebug(){
		String params = "?" + "fn=" + parameters.get(Constants.PARAM_FUNCTION) + "&date=" + parameters.get(Constants.PARAM_DATE) + "&page=" + parameters.get(Constants.PARAM_PAGE) + "&latitude=" + parameters.get(Constants.PARAM_LATITUDE) + "&longitude=" + parameters.get(Constants.PARAM_LONGITUDE);
		String debugUrl = getUrl() + params;
		Log.d("wristband request",debugUrl);
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		return this.parameters;
	}
	
	@Override
	public com.android.volley.Request.Priority getPriority() {
		return Priority.HIGH;
	}

	@Override
	public String getCacheKey() {

		String key = parameters.get(Constants.PARAM_FUNCTION) + "," + parameters.get(Constants.PARAM_DATE) + "," + parameters.get(Constants.PARAM_PAGE);
		return key;
	}

	@Override
	public RetryPolicy getRetryPolicy() {
		return policy;
	}

	public void setTimeout(int seconds){
		policy = new DefaultRetryPolicy(1000 * seconds, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}
	

}
