package com.nimo.wristband.net;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WristbandResponse {
	
	private List<Show> shows;
	private boolean isSuccess;
	
	private String res;
	
	public WristbandResponse(String response){
		res = response;
		isSuccess = true;
		shows = new ArrayList<Show>();
		JSONObject json = null;
		try {
			json = new JSONObject(response);
		} catch (JSONException e) {
			isSuccess = false;
			e.printStackTrace();
		}
		if(isSuccess){
			JSONArray array = json.optJSONArray("shows");
			
			for(int i = 0; i < array.length(); ++i){
				Show show = new Show(array.optJSONObject(i));
				shows.add(show);
			}
		}
		isSuccess = json.optBoolean("execution", false);
	}
	
	public boolean isSuccess(){
		return isSuccess;
	}
	
	public List<Show> getShows(){
		return shows;
	}
	
	@Override
	public String toString() {
		return res;
	}

}
