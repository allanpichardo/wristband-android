package com.nimo.wristband.net;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class Show {
	
	private JSONObject json;
	
	public Show(JSONObject json){
		this.json = json;
	}
	
	public String getVenueName(){
		return json.optString("venue_name","");
	}
	
	public double getVenueLatitude(){
		return json.optDouble("venue_lat",0);
	}
	
	public double getVenueLongitude(){
		return json.optDouble("venue_lng",0);
	}
	
	public String getDatetime(){
		return json.optString("datetime","");
	}
	
	public Date getDate(){
		Date date = null;
		String dtStart = getDatetime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		try {
		    date = format.parse(dtStart);
		} catch (Exception e) {
		    date = null;
		}
		return date;
	}
	
	public String getDateHuman(){
		Date date = getDate();
		String d = "";
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(2);
		if(date != null){
			Calendar calendar = Calendar.getInstance(Locale.US);
			calendar.setTime(date);
			d += nf.format(calendar.get(Calendar.HOUR))+":"+nf.format(calendar.get(Calendar.MINUTE))+calendar.get(Calendar.AM_PM);
		}
		return d;
	}
	
	public String getSongkickUrl(){
		return json.optString("songkick_url","");
	}
	
	public String getBandName(){
		return json.optString("band_name","");
	}
	
	public String getBandcampUrl(){
		return json.optString("bandcamp_url","");
	}
	
	public String getAlternateUrl(){
		return json.optString("alternate_url","");
	}
	
	public String getSmallArtUrl(){
		return json.optString("small_art_url","");
	}
	
	public String getLargeArtUrl(){
		return json.optString("large_art_url","");
	}
	
	public String getAlbumTitle(){
		return json.optString("album_title","");
	}
	
	public String getAlbumUrl(){
		return json.optString("album_url","");
	}
	
	public String getTrackTitle(){
		return json.optString("track_title","");
	}
	
	public String getTrackStreamingUrl(){
		return json.optString("track_streaming_url","");
	}
	
	public String getTrackUrl(){
		return json.optString("track_url","");
	}
	
	public String getBandId(){
		return json.optString("band_id","");
	}
	
	public String getPollPage(){
		return json.optString("poll_page","");
	}
	
	public String getPollDate(){
		return json.optString("poll_date","");
	}
	
	@Override
	public String toString() {
		return json.toString();
	}
	
	public LatLng getLocation(){
		return new LatLng(getVenueLatitude(),getVenueLongitude());
	}

}
