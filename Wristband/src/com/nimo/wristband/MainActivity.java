package com.nimo.wristband;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.nimo.wristband.adapters.ShowListAdapter;
import com.nimo.wristband.adapters.ShowListAdapter.ButtonCallbacks;
import com.nimo.wristband.adapters.ShowListAdapter.OnLoadingListener;
import com.nimo.wristband.db.SimpleStorage;
import com.nimo.wristband.fragments.MusicPlayerFragment;
import com.nimo.wristband.fragments.MusicPlayerFragment.MusicPlayerCallbacks;
import com.nimo.wristband.net.Show;

public class MainActivity extends FragmentActivity implements ConnectionCallbacks, 
OnConnectionFailedListener, OnLoadingListener, ButtonCallbacks, MusicPlayerCallbacks {

	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private LocationClient locationClient;
	private Context context;

	private ListView showList;
	private TextView dateText;
	private ProgressBar progressBar;
	private FrameLayout playerLayout;

	private MusicPlayerFragment musicPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		context = this;

		showList = (ListView)findViewById(R.id.showList);
		dateText = (TextView)findViewById(R.id.dateText);
		progressBar = (ProgressBar)findViewById(R.id.progressBar);
		playerLayout = (FrameLayout)findViewById(R.id.playerLayout);

		musicPlayer = new MusicPlayerFragment();
		musicPlayer.setMusicPlayerCallbacks(this);

		locationClient = new LocationClient(this,this,this);

		init();
	}

	private void init(){
		//set up music player fragment
		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.replace(playerLayout.getId(), musicPlayer);
		fragmentTransaction.setCustomAnimations(R.animator.fadein, R.animator.fadeout, R.animator.fadein, R.animator.fadeout);
		fragmentTransaction.commit();
		
		hidePlayer();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// save index and top position
		final int index = showList.getFirstVisiblePosition();
		View v = showList.getChildAt(0);
		final int top = (v == null) ? 0 : v.getTop();
		SimpleStorage.putScrollPosition(context, index, top);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		final int index = SimpleStorage.getScrollIndex(context);
		final int top = SimpleStorage.getScrollTop(context);
		showList.setSelectionFromTop(index, top);
	}

	private void showPlayer(){
		playerLayout.setVisibility(View.VISIBLE);
	}

	private void hidePlayer(){
		playerLayout.setVisibility(View.GONE);
	}

	// Define a DialogFragment that displays the error dialog
	public static class ErrorDialogFragment extends DialogFragment {
		// Global field to contain the error dialog
		private Dialog mDialog;
		// Default constructor. Sets the dialog field to null
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}
		// Set the dialog to display
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}
		// Return a Dialog to the DialogFragment.
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}


	@Override
	protected void onActivityResult(
			int requestCode, int resultCode, Intent data) {
		// Decide what to do based on the original request code
		switch (requestCode) {
		case CONNECTION_FAILURE_RESOLUTION_REQUEST :
			/*
			 * If the result code is Activity.RESULT_OK, try
			 * to connect again
			 */
			switch (resultCode) {
			case Activity.RESULT_OK :
				/*
				 * Try the request again
				 */
				locationClient.connect();
				break;
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(
						this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			/*
			 * If no resolution is available, display a dialog to the
			 * user with the error.
			 */
			showErrorDialog(connectionResult.getErrorCode());
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		//remove loading spinner
		//attach adapter
		Calendar calendar = Calendar.getInstance(Locale.US);
		DateFormat sdf = SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL, Locale.US);
		//format the date
		String displayDate = sdf.format(calendar.getTime());
		dateText.setText(displayDate);
		ShowListAdapter adapter = new ShowListAdapter(this, locationClient.getLastLocation().getLatitude(), locationClient.getLastLocation().getLongitude(),this);
		showList.setAdapter(adapter);
		showList.setOnScrollListener(adapter);
		adapter.setButtonCallbacks(this);
		
		final int index = SimpleStorage.getScrollIndex(context);
		final int top = SimpleStorage.getScrollTop(context);
		showList.setSelectionFromTop(index, top);
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	void showErrorDialog(int code) {
		GooglePlayServicesUtil.getErrorDialog(code, this, 
				CONNECTION_FAILURE_RESOLUTION_REQUEST).show();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Connect the client.
		locationClient.connect();
	}

	/*
	 * Called when the Activity is no longer visible.
	 */
	@Override
	protected void onStop() {
		// Disconnecting the client invalidates it.
		locationClient.disconnect();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_menu, menu);

		return true;
	}
	
	private void showAbout(){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("About Wristband");
		String message = "Discover some new music tonight! Wristband helps you find live music near you. Take a listen, get directions, and you're in the front row.\n\n\n"+
		"This product uses information from Songkick and Bandcamp.\n\n\n"+
		" © Allan Pichardo 2014.";
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}
	
	private void showSettings(){
		Intent intent = new Intent(this,SettingsActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.aboutItem:
			showAbout();
			return true;
		case R.id.settingsItem:
			showSettings();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void changeDate(){

		final Calendar calendar = Calendar.getInstance(Locale.US);
		DatePickerDialog dialog = new DatePickerDialog(context, new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				calendar.set(year, monthOfYear, dayOfMonth);
				NumberFormat nf = NumberFormat.getIntegerInstance(Locale.US);
				nf.setMinimumIntegerDigits(2);
				DateFormat sdf = SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL, Locale.US);
				//format the date
				String displayDate = sdf.format(calendar.getTime());
				String pollDate = year+"-"+nf.format(monthOfYear+1)+"-"+nf.format(dayOfMonth);
				//set the date and
				ShowListAdapter adapter = new ShowListAdapter(context,locationClient.getLastLocation().getLatitude(),locationClient.getLastLocation().getLongitude(),pollDate,MainActivity.this);
				dateText.setText(displayDate);
				showList.setAdapter(adapter);
				showList.setOnScrollListener(adapter);
				adapter.setButtonCallbacks(MainActivity.this);
			}
		}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		dialog.show();
	}

	@Override
	public void onLoading() {
		progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onLoadingComplete() {
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public void onClickPlay(Show show) {
		musicPlayer.setShow(show);
		musicPlayer.setIndex(showList.getFirstVisiblePosition());
	}

	@Override
	public void onClickShare(Show show) {
		Intent intent=new Intent(android.content.Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

		// Add data to the intent, the receiving app will decide what to do with it.
		Calendar calendar = Calendar.getInstance(Locale.US);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String date = "";
		Date parsed = null;
		try {
			parsed = df.parse(show.getPollDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar poll = Calendar.getInstance(Locale.US);
		poll.setTimeInMillis(parsed.getTime());
		
		if(poll.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) && poll.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && poll.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)){
			date = " tonight. ";
		}else{
			date = " on " + SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL).format(parsed) + ". ";
		}
		
		String message = "I'm going to see " + show.getBandName() + " at " + show.getVenueName() + date + "Who's with me?  " + show.getBandcampUrl();
		intent.putExtra(Intent.EXTRA_TEXT, message);
		
		startActivity(Intent.createChooser(intent, "Share band"));
	}

	@Override
	public void onClickInfo(Show show) {
		Intent intent = new Intent(context,DetailActivity.class);
		intent.putExtra(DetailActivity.ARG_LATITUDE, locationClient.getLastLocation().getLatitude());
		intent.putExtra(DetailActivity.ARG_LONGITUDE, locationClient.getLastLocation().getLongitude());
		intent.putExtra(DetailActivity.ARG_SHOW, show.toString());
		startActivity(intent);
	}

	@Override
	public void onShowPlayer() {
		showPlayer();
	}

	@Override
	public void onHidePlayer() {
		hidePlayer();
	}

	@Override
	public void onIconClick() {
		showList.setSelectionFromTop(musicPlayer.getIndex(), 0);
	}

}
