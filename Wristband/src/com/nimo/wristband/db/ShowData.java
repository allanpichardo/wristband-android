package com.nimo.wristband.db;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.nimo.wristband.constants.Constants;
import com.nimo.wristband.net.Show;
import com.nimo.wristband.net.WristbandRequest;
import com.nimo.wristband.net.WristbandResponse;

public class ShowData extends SQLiteOpenHelper{

	public static final String DATABASE_NAME = "show_data";
	public static final int DATABASE_VERSION = 1;

	public static final String TABLE = "shows";
	public static final String CREATE_STATEMENT = "CREATE TABLE "+ShowData.TABLE+" (_id INTEGER PRIMARY KEY, venue_name TEXT, venue_lat NUMERIC, venue_lng NUMERIC, datetime TEXT, songkick_url TEXT, band_name TEXT, bandcamp_url TEXT, alternate_url TEXT, small_art_url TEXT, large_art_url TEXT, album_title TEXT, album_url TEXT, track_title TEXT, track_streaming_url TEXT, track_url TEXT, band_id TEXT, poll_date TEXT, poll_page TEXT, UNIQUE(band_id,poll_date) ON CONFLICT IGNORE)";

	public static final String KEY_VENUE_NAME = "venue_name";
	public static final String KEY_VENUE_LAT = "venue_lat";
	public static final String KEY_VENUE_LNG = "venue_lng";
	public static final String KEY_DATETIME = "datetime";
	public static final String KEY_SONGKICK_URL = "songkick_url";
	public static final String KEY_BAND_NAME = "band_name";
	public static final String KEY_BANDCAMP_URL = "bandcamp_url";
	public static final String KEY_ALTERNATE_URL = "alternate_url";
	public static final String KEY_SMALL_ART_URL = "small_art_url";
	public static final String KEY_LARGE_ART_URL = "large_art_url";
	public static final String KEY_ALBUM_TITLE = "album_title";
	public static final String KEY_ALBUM_URL = "album_url";
	public static final String KEY_TRACK_TITLE = "track_title";
	public static final String KEY_TRACK_STREAMING_URL = "track_streaming_url";
	public static final String KEY_TRACK_URL = "track_url";
	public static final String KEY_BAND_ID = "band_id";
	public static final String KEY_POLL_DATE = "poll_date";
	public static final String KEY_POLL_PAGE = "poll_page";

	private String pollDate;
	private Context context;
	private OnShowsLoadedListener listener;

	public ShowData(Context context,String pollDate) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.pollDate = pollDate;

		this.context = context;
	}

	public ShowData(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumIntegerDigits(2);

		Calendar calendar = Calendar.getInstance(Locale.US);
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		String month = nf.format(calendar.get(Calendar.MONTH)+1);
		String date = nf.format(calendar.get(Calendar.DATE));
		this.pollDate = year+"-"+month+"-"+date;

		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_STATEMENT);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+TABLE);
		this.onCreate(db);
	}

	private Cursor getRows(){
		SQLiteDatabase db = getWritableDatabase();

		String query = "select * from shows where poll_date = ?";
		String[] args = {pollDate};
		Cursor cursor = db.rawQuery(query, args);

		return cursor;
	}

	public void setPollDate(String pollDate) {
		this.pollDate = pollDate;
	}

	public List<Show> getShows(){

		Cursor cursor = getRows();
		List<Show> shows = new ArrayList<Show>();

		while(cursor.moveToNext()){
			JSONObject json = new JSONObject();
			try {
				json.put(KEY_ALBUM_TITLE, cursor.getString(cursor.getColumnIndex(KEY_ALBUM_TITLE)));
				json.put(KEY_ALBUM_URL, cursor.getString(cursor.getColumnIndex(KEY_ALBUM_URL)));
				json.put(KEY_ALTERNATE_URL, cursor.getString(cursor.getColumnIndex(KEY_ALTERNATE_URL)));
				json.put(KEY_BAND_ID, cursor.getString(cursor.getColumnIndex(KEY_BAND_ID)));
				json.put(KEY_BAND_NAME, cursor.getString(cursor.getColumnIndex(KEY_BAND_NAME)));
				json.put(KEY_BANDCAMP_URL, cursor.getString(cursor.getColumnIndex(KEY_BANDCAMP_URL)));
				json.put(KEY_DATETIME, cursor.getString(cursor.getColumnIndex(KEY_DATETIME)));
				json.put(KEY_LARGE_ART_URL, cursor.getString(cursor.getColumnIndex(KEY_LARGE_ART_URL)));
				json.put(KEY_POLL_DATE, cursor.getString(cursor.getColumnIndex(KEY_POLL_DATE)));
				json.put(KEY_POLL_PAGE, cursor.getString(cursor.getColumnIndex(KEY_POLL_PAGE)));
				json.put(KEY_SMALL_ART_URL, cursor.getString(cursor.getColumnIndex(KEY_SMALL_ART_URL)));
				json.put(KEY_SONGKICK_URL, cursor.getString(cursor.getColumnIndex(KEY_SONGKICK_URL)));
				json.put(KEY_TRACK_STREAMING_URL, cursor.getString(cursor.getColumnIndex(KEY_TRACK_STREAMING_URL)));
				json.put(KEY_TRACK_TITLE,cursor.getString(cursor.getColumnIndex(KEY_TRACK_TITLE)));
				json.put(KEY_TRACK_URL,cursor.getString(cursor.getColumnIndex(KEY_TRACK_URL)));
				json.put(KEY_VENUE_LAT, cursor.getString(cursor.getColumnIndex(KEY_VENUE_LAT)));
				json.put(KEY_VENUE_LNG,cursor.getString(cursor.getColumnIndex(KEY_VENUE_LNG)));
				json.put(KEY_VENUE_NAME,cursor.getString(cursor.getColumnIndex(KEY_VENUE_NAME)));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Show show = new Show(json);
			shows.add(show);
		}

		cursor.close();
		return shows;
	}

	public void addShows(List<Show> shows){

		for(Show show : shows){
			addShow(show);
		}

	}

	public void addShow(Show show){

		ContentValues values = new ContentValues();
		values.put(KEY_ALBUM_TITLE, show.getAlbumTitle());
		values.put(KEY_ALBUM_URL, show.getAlbumUrl());
		values.put(KEY_ALTERNATE_URL, show.getAlternateUrl());
		values.put(KEY_BAND_ID, show.getBandId());
		values.put(KEY_BAND_NAME, show.getBandName());
		values.put(KEY_BANDCAMP_URL, show.getBandcampUrl());
		values.put(KEY_DATETIME, show.getDatetime());
		values.put(KEY_LARGE_ART_URL, show.getLargeArtUrl());
		values.put(KEY_POLL_DATE, show.getPollDate());
		values.put(KEY_POLL_PAGE, show.getPollPage());
		values.put(KEY_SMALL_ART_URL, show.getSmallArtUrl());
		values.put(KEY_SONGKICK_URL, show.getSongkickUrl());
		values.put(KEY_TRACK_STREAMING_URL, show.getTrackStreamingUrl());
		values.put(KEY_TRACK_TITLE,show.getTrackTitle());
		values.put(KEY_TRACK_URL,show.getTrackUrl());
		values.put(KEY_VENUE_LAT, show.getVenueLatitude());
		values.put(KEY_VENUE_LNG,show.getVenueLongitude());
		values.put(KEY_VENUE_NAME,show.getVenueName());

		getWritableDatabase().insertWithOnConflict(TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
	}

	public void fetch(double lat, double lng){

		RequestQueue requestQueue = Volley.newRequestQueue(context);

		int page = getLastPage() + 1;

		Map<String,String> params = new HashMap<String,String>();
		params.put(Constants.PARAM_FUNCTION, Constants.FUNCTION_GET_SHOWS);
		params.put(Constants.PARAM_DATE,pollDate);
		params.put(Constants.PARAM_LATITUDE, String.valueOf(lat));
		params.put(Constants.PARAM_LONGITUDE, String.valueOf(lng));
		params.put(Constants.PARAM_PAGE, String.valueOf(page));

		WristbandRequest request = new WristbandRequest(params,new Listener<String>(){

			@Override
			public void onResponse(String result) {
				WristbandResponse response = new WristbandResponse(result);
				if(response.isSuccess()){
					List<Show> shows = response.getShows();
					if(shows.size() > 0){
						addShows(response.getShows());
					}else{
						Toast.makeText(context, "That's all, folks!", Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(context, "That's all, folks!", Toast.LENGTH_SHORT).show();
				}
				if(listener != null){
					listener.onShowsLoaded(response);
				}
			}

		},new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
				if(listener != null){
					listener.onShowsLoaded(null);
				}
			}

		});
		request.setShouldCache(false);
		request.setTimeout(60);
		requestQueue.add(request);
	}

	public void fetch(double lat, double lng, OnShowsLoadedListener showListener){
		
		this.listener = showListener;

		fetch(lat,lng);
	}

	public int getLastPage(){

		int page = 0;

		Cursor cursor = getRows();
		if(cursor.moveToLast()){
			page = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_POLL_PAGE)));
		}

		return page;
	}
	
	public void wipeAll(){
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE, null, null);
		db.close();
	}

	public void setOnShowsLoadedListener(OnShowsLoadedListener listener) {
		this.listener = listener;
	}

	public interface OnShowsLoadedListener{
		public void onShowsLoaded(WristbandResponse response);
	}

}
