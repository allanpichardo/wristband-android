package com.nimo.wristband.db;

import android.content.Context;
import android.content.SharedPreferences;

public class SimpleStorage {
	
	public static final String PREFS = "wb_prefs";
	
	public static void putScrollPosition(Context context, int index, int top){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, 0);
		sharedPreferences.edit().putInt("scroll_index", index).commit();
		sharedPreferences.edit().putInt("scroll_top", top).commit();
	}
	
	public static int getScrollIndex(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, 0);
		return sharedPreferences.getInt("scroll_index", 0);
	}
	
	public static int getScrollTop(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, 0);
		return sharedPreferences.getInt("scroll_top", 0);
	}

}
