package com.nimo.wristband;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;
import com.nimo.wristband.fragments.WristbandMap;
import com.nimo.wristband.net.Show;

public class DetailActivity extends Activity implements OnMarkerClickListener{
	
	private Context context;
	private ActionBar actionBar;
	
	private WristbandMap map;
	private TextView summaryText;
	private Button bandcampButton;
	private Button songkickButton;
	
	public static final String ARG_LATITUDE = "latitude";
	public static final String ARG_LONGITUDE = "longitude";
	public static final String ARG_SHOW = "show";
	
	public static double myLat = 0.0;
	public static double myLng = 0.0;
	public static double showLat = 0.0;
	public static double showLng = 0.0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail);
		
		context = this;
		actionBar = getActionBar();
		map = (WristbandMap)getFragmentManager().findFragmentById(R.id.map);
		summaryText = (TextView)findViewById(R.id.summaryText);
		bandcampButton = (Button)findViewById(R.id.bandcampButton);
		songkickButton = (Button)findViewById(R.id.songkickButton);
		
		init();
	}
	
	private void init(){
		String s = getIntent().getStringExtra(ARG_SHOW);
		try {
			
			JSONObject json = new JSONObject(s);
			final Show show = new Show(json);
			map.setShow(show);
			map.setOnMarkerClickListener(this);
			
			myLat = getIntent().getDoubleExtra(ARG_LATITUDE, 0);
			myLng = getIntent().getDoubleExtra(ARG_LONGITUDE, 0);
			showLat = show.getVenueLatitude();
			showLng = show.getVenueLongitude();
			
			actionBar.setTitle(show.getVenueName());
			actionBar.setDisplayHomeAsUpEnabled(true);
			
			String summary = show.getBandName() + " @ " + show.getVenueName();
			summary += (show.getDateHuman().isEmpty()) ? "" : " at "+show.getDateHuman();
			summaryText.setText(summary);
			
			bandcampButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
							Uri.parse(show.getBandcampUrl()));
					startActivity(intent);
				}
			});
			
			songkickButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
							Uri.parse(show.getSongkickUrl()));
					startActivity(intent);
				}
			});
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public boolean onMarkerClick(Marker marker) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
				Uri.parse("http://maps.google.com/maps?saddr="+myLat+","+myLng+"&daddr="+showLat+","+showLng));
		startActivity(intent);
		return true;
	}

}
