package com.nimo.wristband.constants;

public class Constants {
	
	public static final String FUNCTION_GET_SHOWS = "get_shows";
	
	public static final String PARAM_LATITUDE = "latitude";
	public static final String PARAM_LONGITUDE = "longitude";
	public static final String PARAM_DATE = "date";
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_BAND = "band";
	public static final String PARAM_FUNCTION = "fn";
	
	public static final String URL_WRISTBAND = "http://54.186.12.213/wristband.php";

}
